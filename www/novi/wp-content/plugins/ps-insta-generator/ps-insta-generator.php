<?php
/*
Plugin Name: Prakticni Saveti Instagram Post Generator
Description: Koristi se za lakše generisanje postova za Instagram
Version: 0.1
Author: Aleksandar Jakovljević
Author URI: https://aleksandarjakovljevic.com/
Text Domain: prakticnisaveti
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

// function that runs when shortcode is called
function ps_insta_generator_shortcode() {

// Things that you want to do.
    $content = file_get_contents(plugin_dir_url( __FILE__ ) . 'includes/canvas-and-form.html');

// Output needs to be return
    return $content;
}
// register shortcode
add_shortcode('ps_insta_generator', 'ps_insta_generator_shortcode');


function ps_insta_generator_enqueue_script() {
    wp_enqueue_script( 'canvas2image', plugin_dir_url( __FILE__ ) . 'js/canvas2image.js' );
    wp_enqueue_script( 'canvas-text-box', plugin_dir_url( __FILE__ ) . 'js/canvas-text-box.js' );
    wp_enqueue_script( 'ps_insta_generator_js', plugin_dir_url( __FILE__ ) . 'js/ps-insta-generator.js' );
    wp_enqueue_style('ps_insta_generator_css', plugin_dir_url( __FILE__ ) . 'css/ps-insta-generator.css');
}
add_action('wp_enqueue_scripts', 'ps_insta_generator_enqueue_script');
/**
 *
 * @type {{writeTextWithBackground: *}}
 */
const canvasTextBox = (function () {

    const defaultOptions = {
        fontSize: 32,
        lineHeight: 40,
        startX: 20,
        startY: 20,
        marginTop: 0,
        marginBottom: 0,
        marginLeft: 0,
        marginRight: 0,
        width: 200,
        textFillStyle: '#FFFFFF',
        useBackground: false,
        backgroundFillStyle: 'rgba(50,50,50,0.5)'
    };

    function checkOptions(options) {
        for (let [key, value] of Object.entries(defaultOptions)) {
            if (typeof (options[key]) == 'undefined' || !(options.key)) {
                options[key] = value;
            }
        }
    }

    const regularTextBox = function (mainCanvas, text, options = {}) {
        checkOptions(options);
        const ctx = mainCanvas.getContext('2d');
        let fontSize = options.fontSize;
        let lineHeight = options.lineHeight;
        let startX = options.startX;
        let startY = options.startY;
        let textBoxWidth = options.width;

        ctx.font = 'normal normal bold ' + fontSize + 'px/' + lineHeight + 'px helvetica neue,Helvetica,segoe ui,Arial,sans-serif';

        let lines = [];
        let lineMeasures = [];
        let maxLineWidth = 0;
        let lineOrd = 0;
        lines[lineOrd] = '';
        let words = text.split(' ');
        for (let i = 0; i < words.length; i++) {
            let currentLineText = lines[lineOrd];
            if (currentLineText == '') {
                currentLineText += words[i];
            } else {
                currentLineText += ' ' + words[i];
            }
            let lineMeasure = ctx.measureText(currentLineText);
            if (lineMeasure.width > textBoxWidth) {
                lines[++lineOrd] = words[i];
            } else {
                lines[lineOrd] = currentLineText;
                lineMeasures[lineOrd] = lineMeasure;
                if(lineMeasure.width > maxLineWidth) {
                    maxLineWidth = lineMeasure.width;
                }
            }
        }

        if(options.bottomAligned) {
            startY = mainCanvas.height - ((lines.length - 1) * (lineHeight) + 18);
        }

        for (let i = 0; i < lines.length; i++) {

            if(options.useBackground) {
                ctx.fillStyle = options.backgroundFillStyle;
                ctx.fillRect(startX - 10, startY - lineHeight + 7, lineMeasures[i].width + 20, lineHeight);
            }

            ctx.fillStyle = options.textFillStyle;
            ctx.fillText(lines[i], startX, startY);

            startY += lineHeight;
        }
    }

    const writeTextWithBackground = function (mainCanvas, text, options = {}) {
        checkOptions(options);
        const ctx = mainCanvas.getContext('2d');
        let fontSize = options.fontSize;
        let lineHeight = options.lineHeight;
        let startX = options.startX;
        let startY = mainCanvas.height - (300 - lineHeight);

        ctx.font = 'normal normal bold ' + fontSize + 'px/' + lineHeight + 'px helvetica neue,Helvetica,segoe ui,Arial,sans-serif';


        let lines = [];
        let lineMeasures = [];
        let maxLineWidth = 0;
        let lineOrd = 0;
        lines[lineOrd] = '';
        let words = text.split(' ');
        for (let i = 0; i < words.length; i++) {
            let currentLineText = lines[lineOrd];
            if (currentLineText == '') {
                currentLineText += words[i];
            } else {
                currentLineText += ' ' + words[i];
            }
            let lineMeasure = ctx.measureText(currentLineText);
            if (lineMeasure.width > mainCanvas.width - startX - 10) {
                lines[++lineOrd] = words[i];
            } else {
                lines[lineOrd] = currentLineText;
                lineMeasures[lineOrd] = lineMeasure;
                if(lineMeasure.width > maxLineWidth) {
                    maxLineWidth = lineMeasure.width;
                }
            }
        }

        startY = mainCanvas.height - ((lines.length - 1) * (lineHeight) + 18);

        for (let i = 0; i < lines.length; i++) {

            if(options.useBackground) {
                ctx.fillStyle = options.backgroundFillStyle;
                ctx.fillRect(startX - 10, startY - lineHeight + 7, lineMeasures[i].width + 20, lineHeight);
            }

            ctx.fillStyle = options.textFillStyle;
            ctx.fillText(lines[i], startX, startY);

            startY += lineHeight;
        }


    };

    return {
        writeTextWithBackground: writeTextWithBackground
    }
}());
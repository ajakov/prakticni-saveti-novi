var drawData = {
    containerId: 'ps_insta_post_container',
    bgImageUrl: 'https://prakticnisaveti.com/wp-content/plugins/ps-insta-generator/img/default-background.jpg',
    watermarkImageUrl: 'https://prakticnisaveti.com/wp-content/plugins/ps-insta-generator/img/prakticni-saveti-logo-okrugli-watermark.png',
    text: 'Ево једна линија лепог текста! И ако затреба још једна линија и само да знате да је ово баш супер ствар!'
};

(function ($) {

    $(document).ready(function () {

        getTextAndDraw();

        $('#ps_insta_post_submit').click(function(){
            getTextAndDraw();
        });

        $('#ps_insta_post_download').click(function(){
            prepareDownload();
        });

        $('#ps_insta_post_upload').on('change', function(ev) {
            var f = ev.target.files[0];
            var fr = new FileReader();

            fr.onload = function(ev2) {
                drawData.bgImageUrl = ev2.target.result;
                getTextAndDraw();
            };

            fr.readAsDataURL(f);
        });

    });

    function prepareDownload() {
        Canvas2Image.saveAsPNG(document.getElementById('ps_insta_post_container'), 'prakticni-savet', 600, 600);
    }

    function getTextAndDraw() {
        drawData.text = $('#ps_insta_post_textarea').val();
        drawPoster(drawData);
    }

    function drawPoster(drawData) {
        var ps_insta_post_canvas_collection = $('#' + drawData.containerId);
        if (ps_insta_post_canvas_collection) {
            var ps_insta_post_canvas = ps_insta_post_canvas_collection[0];
            console.log('Container found!');
            var ctx = ps_insta_post_canvas.getContext('2d');

            ctx.clearRect(0, 0, ps_insta_post_canvas.width, ps_insta_post_canvas.height);

            var background = new Image();
            background.src = drawData.bgImageUrl;
            //background.setAttribute('crossorigin', 'anonymous');

            var watermark = new Image();
            watermark.src = drawData.watermarkImageUrl;
            //watermark.setAttribute('crossorigin', 'anonymous');

            background.addEventListener('load', function() {
                ctx.drawImage(background,0,0, ps_insta_post_canvas.width, ps_insta_post_canvas.height);
                ctx.drawImage(watermark, ps_insta_post_canvas.width - (watermark.width/2) - 20,20, watermark.width/2, watermark.height/2);

                canvasTextBox.writeTextWithBackground(ps_insta_post_canvas, drawData.text);

            }, false);




        }
    }

})(jQuery);
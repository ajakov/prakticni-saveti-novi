<?php
/**
 * The default template for displaying content
 *
 * Used for both singular and index.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

?>
<div class="psaveti2020-column">
    <article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

        <?php

        get_template_part( 'template-parts/entry-header-istaknut' );


        get_template_part( 'template-parts/featured-image-istaknut' );


        ?>



    </article><!-- .post -->
</div>
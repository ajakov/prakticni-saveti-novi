<?php
/**
 * The default template for displaying content
 *
 * Used for both singular and index.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

?>
<div class="psaveti2020-column psaveti2020-archive-column">
    <article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<?php


    if(!has_category(61)) {
        get_template_part( 'template-parts/entry-header-archive' );
    }



	get_template_part( 'template-parts/featured-image-archive' );




	?>



    </article><!-- .post -->
</div>
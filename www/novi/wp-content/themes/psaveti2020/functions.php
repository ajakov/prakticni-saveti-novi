<?php

function psaveti2020_enqueue_styles() {

    $parent_style = 'twentytwenty-style';

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style(
        'psaveti2020-style',
        get_stylesheet_directory_uri() . '/style.css',
        [ $parent_style ],
        wp_get_theme()->get('Version')
    );
    wp_add_inline_style( 'psaveti2020-style', file_get_contents(get_stylesheet_directory_uri() . '/css/inline.css') );
    wp_add_inline_style( 'psaveti2020-style', file_get_contents(get_stylesheet_directory_uri() . '/css/inline-tablet.css') );
    wp_add_inline_style( 'psaveti2020-style', file_get_contents(get_stylesheet_directory_uri() . '/css/inline-mobile.css') );
}
add_action( 'wp_enqueue_scripts', 'psaveti2020_enqueue_styles' );

add_theme_support(
    'custom-logo',
    array(
        'height'      => 120,
        'width'       => 38,
        'flex-height' => true,
        'flex-width'  => true,
        'header-text' => array( 'site-title', 'site-description' ),
    )
);

add_filter('post_class', 'set_featured_post_class', 10,3);
function set_featured_post_class($classes, $class, $post_id){

    //check if some meta field is set
    $istaknut = get_field('istaknut', $post_id);
    if (!empty($istaknut) && !is_singular()) {
        $classes[] = 'istaknut_post'; //add a custom class to highlight this row in the table
    }

    // Return the array
    return $classes;
}
<?php

function readCSV() {

    $csvData = [];
    if (($handle = fopen(__DIR__ . '/media.csv', "r")) !== FALSE) {
        while (($data = fgetcsv($handle, null, ",")) !== FALSE) {
            $csvData[] = $data;
        }
        fclose($handle);
    }

    return $csvData;

}


function writeCSV($csvData) {
    $fp = fopen(__DIR__ . '/media.csv', 'w');

    foreach ($csvData as $fields) {
        fputcsv($fp, $fields);
    }

    fclose($fp);
}

function addIfNotExistingId($fields, &$csvData) {

    $id = $fields[0];
    $existing = false;

    foreach ($csvData as $row) {
        if($row[0] == $id) {
            $existing = true;
            break;
        }
    }

    if(!$existing) {
        $csvData[] = $fields;
    }

}

function getTitleFromRow($row, $numWords = 4) {
    $words = explode(' ', $row[2]);
    $wordsForTitle = array_slice($words, 0, $numWords);
    return implode(' ', $wordsForTitle);
}

function getContentFromRow($row) {
    $content = $row[2];
    $position = strpos($content, 'За још оваквих савета посетите сајт');
    if($position !== false) {
        $content = substr($content, 0, $position);
    }
    return $content;
}

function findHashTags($row) {
    $hashTags = [];
    $forbiddenHashTags = [
        '#PrakticniSaveti',
        '#prakticnisaveti',
        '#lifehacks',
        '#saveti',
        '#badadvice',
        '#funny',
        '#jokes',
    ];
    $content = $row[2];
    $words = explode(' ', $content);
    foreach ($words as $word) {
        if(substr($word, 0, 1) == '#' && !in_array($word, $forbiddenHashTags)) {
            $hashTags[] = substr($word, 1);
        }
    }
    return $hashTags;
}

function transliterate($string) {
    $search = ['nj','lj','dj','dž','NJ','Nj','LJ','Lj','DJ','Dj','DŽ','Dž','a','b','v','g','d','e','ž','z','i','j','k','l','m','n','o','p','r','s','t','ć','u','f','h','c','č','š','A','B','V','G','D','E','Ž','Z','I','J','K','L','M','N','O','P','R','S','T','Ć','U','F','H','C','Č','Š', '_'];
    $replace = ['њ','љ','ђ','џ','Њ','Њ','Љ','Љ','Ђ','Ђ','Џ','Џ','а','б','в','г','д','е','ж','з','и','ј','к','л','м','н','о','п','р','с','т','ћ','у','ф','х','ц','ч','ш','A','Б','В','Г','Д','Е','Ж','З','И','Ј','К','Л','М','Н','О','П','Р','С','Т','Ћ','У','Ф','Х','Ц','Ч','Ш', ' '];

    return str_replace($search, $replace, $string);
}
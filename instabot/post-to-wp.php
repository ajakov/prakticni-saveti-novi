<?php

require __DIR__ . '/functions.php';
require_once(__DIR__ . "/../www/novi/wp-load.php");
include_once( ABSPATH . 'wp-admin/includes/image.php' );

$csvData = readCSV();
$i = 0;
foreach ($csvData as $key => $row) {
    $i++;
    if($i == 1) continue;
    if($row[5] == 1) continue;

    /*******************************************************
     ** POST VARIABLES
     *******************************************************/

    $postType = 'post'; // set to post or page
    $userID = 1; // set to user id
    $categoryID = '61'; // set to category id.
    $postStatus = 'publish';  // set to future, draft, or publish

    $leadTitle = 'Инстаграм: ' . getTitleFromRow($row) . ' ...';

    $leadContent = getContentFromRow($row);

    $timeStamp = date('Y-m-d H:i:s', strtotime($row[3]));

    $new_post = array(
        'post_title' => $leadTitle,
        'post_content' => $leadContent,
        'post_status' => $postStatus,
        'post_date' => $timeStamp,
        'post_author' => $userID,
        'post_type' => $postType,
        'post_category' => array($categoryID)
    );

    $postId = wp_insert_post($new_post);

    $tagsFromHashTags = findHashTags($row);
    $transliteratedTags = [];
    foreach ($tagsFromHashTags as $tag) {
        $transliteratedTags[] = transliterate($tag);
    }

    wp_set_post_tags($postId, $transliteratedTags);

    add_post_meta($postId,'Instagram_ID', $row[0]);
    add_post_meta($postId,'Instagram_URL', $row[1]);

    // image to be uploaded to WordPress and set as featured image
    $IMGFileName = 'img-from-instagram-' . $row[0] . '.jpg';

    //prepare upload image to WordPress Media Library
    $upload = wp_upload_bits($IMGFileName , null, file_get_contents($row[1]));

    // check and return file type
    $imageFile = $upload['file'];
    $wpFileType = wp_check_filetype($imageFile, null);

    // Attachment attributes for file
    $attachment = array(
        'post_mime_type' => $wpFileType['type'],  // file type
        'post_title' => sanitize_file_name($imageFile),  // sanitize and use image name as file name
        'post_content' => '',  // could use the image description here as the content
        'post_status' => 'inherit'
    );

    // insert and return attachment id
    $attachmentId = wp_insert_attachment( $attachment, $imageFile, $postId );

    // insert and return attachment metadata
    $attachmentData = wp_generate_attachment_metadata( $attachmentId, $imageFile);

    // update and return attachment metadata
    wp_update_attachment_metadata( $attachmentId, $attachmentData );

    // finally, associate attachment id to post id
    $success = set_post_thumbnail( $postId, $attachmentId );

    // was featured image associated with post?
    if($success){

        $csvData[$key][5] = 1;

    }

}

writeCSV($csvData);
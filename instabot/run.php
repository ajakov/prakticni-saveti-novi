<?php

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/Instagram.php';
require __DIR__ . '/functions.php';

use App\Instagram;

$accessToken = file_get_contents('token.txt');
$instagram = new Instagram($accessToken);

$mediaJson = (json_decode($instagram->getMedia(), true));
//print_r(json_decode($instagram->getSingleMedia('17885960083469969', Instagram::DEFAULT_CAROUSEL_CHILDREN_FIELDS), true));
//print_r($mediaJson);
$csvData = readCSV();

foreach ($mediaJson['data'] as $currentPost) {
    if($currentPost['media_type'] != 'IMAGE') {
        continue;
    }
    $fieldsForCurrentPost = [
        $currentPost['id'],
        $currentPost['media_url'],
        $currentPost['caption'],
        $currentPost['timestamp'],
        $currentPost['permalink'],
        0,
    ];

    addIfNotExistingId($fieldsForCurrentPost, $csvData);
}

writeCSV($csvData);
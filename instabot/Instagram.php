<?php

namespace App;

use GuzzleHttp\Client;

class Instagram
{
    const INSTAGRAM_API_URL = 'https://graph.instagram.com';
    const DEFAULT_MEDIA_FIELDS = [
        'id',
        'caption',
        'media_type',
        'media_url',
        'username',
        'timestamp',
        'children',
        'permalink',
    ];
    const DEFAULT_CAROUSEL_CHILDREN_FIELDS = [
        'id',
        'media_type',
        'media_url',
        'username',
        'timestamp',
        'children',
        'permalink',
    ];
    protected $accessToken;
    protected $client;

    public function __construct(string $accessToken)
    {
        $this->accessToken = $accessToken;
        $this->client = new Client();
    }

    public function getMedia(array $fields = self::DEFAULT_MEDIA_FIELDS)
    {
        $response = $this->client->request('GET', self::INSTAGRAM_API_URL . '/me/media?fields=' . implode(',', $fields) . '&access_token=' . $this->accessToken);
        return $response->getBody();
    }

    public function getSingleMedia(string $igMediaId, array $fields = self::DEFAULT_MEDIA_FIELDS)
    {
        $response = $this->client->request('GET', self::INSTAGRAM_API_URL . '/' . $igMediaId . '?fields=' . implode(',', $fields) . '&access_token=' . $this->accessToken);
        return $response->getBody();
    }

    public function refreshTokenIfNeeded()
    {
        $response = $this->client->request('GET', self::INSTAGRAM_API_URL . '/refresh_access_token?grant_type=ig_refresh_token&access_token=' . $this->accessToken);
        $response = json_decode($response->getBody()->getContents(), true);
        if (isset($response['expires_in']) && $response['expires_in'] < 86400) {
            $newAccessToken = $response['access_token'];
            $this->setNewToken($newAccessToken);
        }
    }

    public function setNewToken($newAccessToken)
    {
        $this->accessToken = $newAccessToken;
        file_put_contents('token.txt', $newAccessToken);
    }

}
<?php

require __DIR__ . '/functions.php';
require_once("../www/novi/wp-load.php");
include_once( ABSPATH . 'wp-admin/includes/image.php' );

$all_instagram_posts = get_posts([
    'numberposts'   => -1,
    'post_type'		=> 'post',
    'category' => 61,
]);

foreach ($all_instagram_posts as $currentPost) {
    echo $currentPost->ID . "\n";
    wp_delete_post($currentPost->ID, true);
}

$csvData = readCSV();
$i = 0;
foreach ($csvData as $key => $row) {
    $i++;
    if ($i == 1) continue;
    $csvData[$key][5] = 0;
}
writeCSV($csvData);
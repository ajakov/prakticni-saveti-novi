<?php

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/functions.php';

use InstagramScraper\Instagram;
use Phpfastcache\Helper\Psr16Adapter;

$instagram = \InstagramScraper\Instagram::withCredentials('prakticnisaveti', 'howmuch4ever',  new Psr16Adapter('Files'));
$instagram->login();

$nonPrivateAccountMedias = $instagram->getMedias('prakticnisaveti', 5);

$csvData = readCSV();

foreach ($nonPrivateAccountMedias as $currentPost) {
    /** @var \InstagramScraper\Model\Media $currentPost */
    $fieldsForCurrentPost = [
        $currentPost->getId(),
        $currentPost->getImageHighResolutionUrl(),
        $currentPost->getCaption(),
        $currentPost->getCreatedTime(),
        $currentPost->getLink(),
        0,
    ];

    addIfNotExistingId($fieldsForCurrentPost, $csvData);
}

writeCSV($csvData);


